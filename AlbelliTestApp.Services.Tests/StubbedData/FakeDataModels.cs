﻿using System;
using System.Collections.Generic;
using AlbelliTestApp.Shared.Models;

namespace AlbelliTestApp.Services.Tests.StubbedData
{
    class FakeDataModels
    {
        public static ICollection<CustomerModel> CustomersList => new List<CustomerModel>
            {
                new CustomerModel
                {
                    Id = new Guid("07e2d447-bd76-4bf3-9a94-7bd0ae47fa4f"),
                    Name = "Some Name 1",
                    Email = "email1@domain.com"
                },
                new CustomerModel
                {
                    Id = new Guid("e007ab76-4ac3-4e45-8267-16e841cc7636"),
                    Name = "Some Name 2",
                    Email = "email2@domain.com"
                },
                new CustomerModel
                {
                    Id = new Guid("37f0081d-b7dd-4b1c-bff4-5a68381fa228"),
                    Name = "Some Name 3",
                    Email = "email3@domain.com"
                },
            };

        public static ICollection<OrderModel> OrdersList =>
            new List<OrderModel>
            {
                new OrderModel
                {
                    Id = new Guid("494862cf-26a2-414b-95d1-6b59f7c83a4b"),
                    Amount = 10.50m,
                    CreatedDate = new DateTimeOffset(2016, 10, 13, 14, 0, 0, 0, new TimeSpan(3, 0,0))
                },
                new OrderModel
                {
                    Id = new Guid("74ac1af6-1c63-4aa5-acee-e61cbcd62bd7"),
                    Amount = 100.50m,
                    CreatedDate = new DateTimeOffset(2016, 10, 13, 12, 0, 0, 0, new TimeSpan(3, 0,0))
                }
            };
    }
}
