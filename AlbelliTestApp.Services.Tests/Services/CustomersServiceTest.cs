﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AlbelliTestApp.DataAccess.Interfaces;
using AlbelliTestApp.DataAccess.Models;
using AlbelliTestApp.Shared.Interfaces;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NUnit.Framework;
using NUnit.Framework.Internal;
using AlbelliTestApp.Services;
using AlbelliTestApp.Services.Tests.StubbedData;
using AlbelliTestApp.Shared.Models;

namespace AlbelliTestApp.Services.Tests.Services
{
    [TestFixture]
    public class CustomersServiceTest
    {
        [SetUp]
        public void Initialize()
        {
            _customerRepositoryMock = new Mock<IExtendedRepository<Customer>>();

            _orderRepositoryMock = new Mock<IExtendedRepository<Order>>();

            #region Mock of generic repository methods
            //GetAll
            _customerRepositoryMock.Setup(r => r.GetAll()).Returns(() =>
                {
                    var customers = _customersList;
                    foreach (var customer in customers)
                    {
                        customer.Orders = new HashSet<Order>();
                    }
                    return customers;
                }
                );

            //GetById
            _customerRepositoryMock.Setup(s => s.GetById(It.IsAny<Guid>()))
                .Returns<Guid>(
                    id =>
                            _customersList.SingleOrDefault(m => m.Id == id)
                );

            //Add new customer
            _customerRepositoryMock.Setup(s => s.Insert(It.IsAny<Customer>()))
                .Returns<Customer>(customer =>
                {
                    if(customer.Id == Guid.Empty)
                        customer.Id = Guid.NewGuid();

                    _customersList.Add(customer);

                    return customer;
                });

            //Update existing customer
            _customerRepositoryMock.Setup(s => s.Update(It.IsAny<Customer>()))
                .Returns<Customer>(customer =>
                {
                    //get customer for update its orders
                    var customerToUpdate = _customersList.SingleOrDefault(m => m.Id ==customer.Id);

                    //if customer is not found throw new exception
                    if (customerToUpdate == null) throw new Exception();

                    //remove all from list
                    _customersList.Remove(customerToUpdate);

                    //get customer orders
                    var orders = customer.Orders;

                    //assign new Id for recent added orders
                    foreach (var order in orders)
                    {
                        if(order.Id == Guid.Empty)
                            order.Id = Guid.NewGuid();
                    }

                    //assign proceed orders to customer
                    customer.Orders = orders;

                    //save customer data with new orders
                    _customersList.Add(customer);

                    return customer;
                });
            #endregion

            #region Mock of extended repository methods

            //GetById
            _customerRepositoryMock.Setup(s => s.GetById(It.IsAny<Guid>(), It.IsAny<Expression<Func<Customer, object>>[]>()))
                .Returns<Guid, Expression<Func<Customer, object>>[]>(
                    (id, includes) =>
                    {
                        var customer = _customersList.SingleOrDefault(m => m.Id == id);

                        if (customer == null) throw new NullReferenceException();

                        if(includes.Any())
                            customer.Orders = _orderList;

                        return customer;
                    }
                );
            #endregion

            #region Order RepositoryMock
            //add new order
            _orderRepositoryMock.Setup(s => s.Insert(It.IsAny<Order>())).Returns<Order>(
                order =>
                {
                    //get customer for update its orders
                    var customerToUpdate = _customersList.SingleOrDefault(m => m.Id == order.CustomerId);

                    //if customer is not found throw new exception
                    if (customerToUpdate == null) throw new Exception();

                    if(order.Id == Guid.Empty)
                        order.Id = Guid.NewGuid();

                    //remove all from list
                    _customersList.Remove(customerToUpdate);

                    //assign proceed orders to customer
                    customerToUpdate.Orders.Add(order);

                    //save customer data with new orders
                    _customersList.Add(customerToUpdate);

                    return order;
                }
            );
            #endregion
        }

        private Mock<IExtendedRepository<Customer>> _customerRepositoryMock;

        private Mock<IExtendedRepository<Order>> _orderRepositoryMock;

        private ICustomersService Service => new CustomersService(_customerRepositoryMock.Object, _orderRepositoryMock.Object);

        private readonly ICollection<Customer> _customersList = FakeDataEntities.CustomersList;

        private readonly ICollection<CustomerModel> _customerModels = FakeDataModels.CustomersList;

        private readonly ICollection<OrderModel> _orderModels = FakeDataModels.OrdersList;

        private readonly ICollection<Order> _orderList = FakeDataEntities.OrdersList;

        [Test]
        public void GetCustomerByIdIncludingOrders()
        {
            // Arrange
            var customer = _customerModels.FirstOrDefault();
            customer.Should().NotBeNull();

            // Act
            var returnedCustomer = Service.GetById(customerId: customer.Id, includeOrders:true);

            // Assert
            returnedCustomer.Should().NotBeNull();
            returnedCustomer.Orders.Should().NotBeEmpty();

            returnedCustomer.Orders.ShouldAllBeEquivalentTo(_orderModels);
        }

        [Test]
        public void GetAllCustomersWithoutOrders()
        {
            // Arrange
            var allCustomers = _customerModels;

            // Act
            var returnedCustomers = Service.GetAll();

            // Assert
            var customerModels = returnedCustomers as IList<CustomerModel> ?? returnedCustomers.ToList();

            allCustomers.ShouldAllBeEquivalentTo(customerModels, opt => opt.Excluding(m => m.Orders));

            //check all orders for each customer
            foreach (var returnedCustomer in customerModels)
            {
                returnedCustomer.Orders.Should().BeEmpty();
            }
        }

        [Test]
        public void AddNewCustomer()
        {
            // Arrange
            var customer = new CustomerModel
            {
                Id = Guid.NewGuid(),
                Email = "newlycreated@domain.com",
                Name = "newlycreated"
            };

            _customerModels.Add(customer);

            // Act
            var id = Service.Add(customer);

            // Assert
            id.Should().NotBeEmpty();

            customer.Id = id;

            var found = _customersList.SingleOrDefault(m => m.Id == customer.Id);

            found.Should().NotBeNull();

            found.Orders.Should().BeEmpty();

            found.ShouldBeEquivalentTo(customer, opt => opt.Excluding(m => m.Orders));
        }

        [Test]
        public void AddNewOrderToCustomer()
        {
            // Arrange
            // Arrange
            var customer = _customersList.FirstOrDefault();
            customer.Should().NotBeNull();

            var order = new OrderModel
            {
                Id = Guid.NewGuid(),
                Amount = 15.20m,
                CreatedDate = DateTimeOffset.UtcNow.AddMinutes(-30)
            };

            // Act
            Service.AddOrderToCustomer(customer.Id, order);

            // Assert
            var found = _customersList.SingleOrDefault(m => m.Id == customer.Id);

            found.Should().NotBeNull();

            found.Orders.Should().NotBeNull();

            var foundOrder =
                found.Orders.FirstOrDefault(o => (o.Amount == order.Amount) && (o.CreatedDate == order.CreatedDate));

            foundOrder.Should().NotBeNull();

            foundOrder.Id.Should().NotBeEmpty().Should().NotBe(Guid.Empty);

            foundOrder.ShouldBeEquivalentTo(order, opt => opt.Excluding(e => e.Id).Excluding(e => e.Customer).Excluding(e => e.CustomerId));
        }
    }
}
