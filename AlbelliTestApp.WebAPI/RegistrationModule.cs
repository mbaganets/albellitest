﻿using System.Configuration;
using AlbelliTestApp.DataAccess.Contexts;
using AlbelliTestApp.DataAccess.Interfaces;
using AlbelliTestApp.DataAccess.Models;
using AlbelliTestApp.DataAccess.Repositories;
using AlbelliTestApp.DataAccess.Repositories.Dapper;
using AlbelliTestApp.Services;
using AlbelliTestApp.Shared.Interfaces;
using Autofac;

namespace AlbelliTestApp.WebAPI
{
    public class RegistrationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            RegisterRepository(builder);

            RegisterServices(builder);
        }

        private static void RegisterRepository(ContainerBuilder builder)
        {
            bool useDapperValue = false;
            var useDapperSetting = ConfigurationManager.AppSettings["useDapper"];

            bool.TryParse(useDapperSetting, out useDapperValue);

            if(!useDapperValue)
            {

                builder.RegisterType<ContextFabric>().As<IContextFabric>();

                builder.RegisterType<GenericRepository<Order>>().As<IGenericRepository<Order>>();
                builder.RegisterType<ExtendedRepository<Customer>>().As<IExtendedRepository<Customer>>();
            }
            else
            {
                builder.RegisterType<OrdersDapperRepository>().As<IGenericRepository<Order>>();
                builder.RegisterType<CustomersDapperRepository>().As<IExtendedRepository<Customer>>();
            }
        }

        private static void RegisterServices(ContainerBuilder builder)
        {
            builder.RegisterType<CustomersService>().As<ICustomersService>();
        }
    }
}