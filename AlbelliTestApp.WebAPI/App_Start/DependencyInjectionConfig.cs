﻿using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;

namespace AlbelliTestApp.WebAPI
{
    public class DependencyInjectionConfig
    {
        public static void Configure(HttpConfiguration config)
        {
            // Create the container builder.
            var builder = new ContainerBuilder();

            // Register the Web API controllers.
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            //register all services and repositories for it
            builder.RegisterModule<RegistrationModule>();

            //build container
            var container = builder.Build();

            //create WebApi dependency resolver
            var resolver = new AutofacWebApiDependencyResolver(container);

            //assign AutoFac as dependency resolver
            config.DependencyResolver = resolver;
        }
    }
}