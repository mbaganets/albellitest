﻿using System.Net.Http.Headers;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace AlbelliTestApp.WebAPI
{
    public static class SerializerConfig
    {
        public static void Configure(HttpConfiguration config)
        {
            //respond JSON to just plain text/html
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));

            var jsonSettings = config.Formatters.JsonFormatter.SerializerSettings;

            jsonSettings.Formatting = Formatting.Indented;

            jsonSettings.PreserveReferencesHandling = PreserveReferencesHandling.Objects;

            jsonSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            //remove $id during JSON serialization
            jsonSettings.PreserveReferencesHandling = PreserveReferencesHandling.None;

            //ignore empty values while serialization
            jsonSettings.NullValueHandling = NullValueHandling.Ignore;

            jsonSettings.Converters.Add(new StringEnumConverter());

            //uncomment to prevent using XML format responses
            //config.Formatters.Remove(config.Formatters.XmlFormatter);
        }
    }
}