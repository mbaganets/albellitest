﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Http;
using AlbelliTestApp.Shared.Interfaces;
using AlbelliTestApp.Shared.Models;
using Newtonsoft.Json;

namespace AlbelliTestApp.WebAPI.Controllers
{
    [Route("api/v1/customers/{id?}")]
    public class CustomersController : ApiController
    {
        private readonly ICustomersService _customersService;

        public CustomersController(ICustomersService customersService)
        {
            _customersService = customersService;
        }

        /// <summary>
        ///     return list of all customers without its orders
        /// </summary>
        /// <returns></returns>
        // GET: api/Customers
        public IEnumerable<CustomerModel> Get()
        {
            try
            {
                //get all customers from service
                return _customersService.GetAll();
            }
            catch (Exception ex)
            {
                //TODO:log exception somehow
                throw new HttpException(500, ex.Message);
            }
        }

        /// <summary>
        ///     return customer by Id and his Orders list
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Customers/5
        public CustomerModel Get(Guid id)
        {
            try
            {
                //get customer from service including orders
                return _customersService.GetById(customerId: id, includeOrders: true);
            }
            catch (Exception ex)
            {
                //TODO:log exception somehow
                throw new HttpException(500, ex.Message);
            }
        }

        /// <summary>
        ///     add new Customer
        /// </summary>
        /// <param name="value"></param>
        // POST: api/Customers
        public Guid Post(CustomerModel value)
        {
            try
            {
                //try to add customer
                return _customersService.Add(value);
            }
            catch (Exception ex)
            {
                //TODO:log exception somehow
                throw new HttpException(500, ex.Message);
            }
        }

        /// <summary>
        ///     update current customer with his order
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        // PUT: api/Customers/5
        public void Put(Guid id, OrderModel value)
        {
            try
            {
                //update order for customer
                _customersService.AddOrderToCustomer(id, value);
            }
            catch (Exception ex)
            {
                //TODO:log exception somehow
                throw new HttpException(500, ex.Message);
            }
        }

        /// <summary>
        ///     Delete customer
        ///     Not implemented exception going to throw
        /// </summary>
        /// <param name="id"></param>
        // DELETE: api/Customers/5
        public void Delete(Guid id)
        {
            try
            {
                _customersService.Delete(new CustomerModel
                {
                    Id = id
                });
            }
            catch (Exception ex)
            {
                //TODO:log exception somehow
                //send internal server error
                //501 Not implemented for delete
                throw new HttpException(501, ex.Message);
            }
        }
    }
}