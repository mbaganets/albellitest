﻿using System;
using System.Collections.Generic;
using System.Linq;
using AlbelliTestApp.Shared.Interfaces;
using AlbelliTestApp.Shared.Models;
using AlbelliTestApp.WebAPI.Controllers;
using AlbelliTestApp.WebAPI.Tests.StubbedData;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace AlbelliTestApp.WebAPI.Tests.Controllers
{
    [TestFixture]
    public class CustomersControllerTest
    {
        [SetUp]
        public void Initialize()
        {
            _customerServiceMock = new Mock<ICustomersService>();

            //GetAll
            _customerServiceMock.Setup(s => s.GetAll()).Returns(_customerModels);

            //GetById
            _customerServiceMock.Setup(s => s.GetById(It.IsAny<Guid>()))
                .Returns<Guid>(
                    id =>
                            _customerModels.SingleOrDefault(m => m.Id == id)
                );


            //GetById with or without Orders data
            _customerServiceMock.Setup(s => s.GetById(It.IsAny<Guid>(), It.IsAny<bool>()))
                .Returns<Guid, bool>(
                    (id, includeOrders) =>
                    {
                        //get customer
                        var customer = _customerModels.SingleOrDefault(m => m.Id == id);

                        //if customer is null throw exception
                        if (customer == null) return null;

                        //if it have to return orders add orders data to customer
                        customer.Orders = includeOrders ? _ordersModel : new List<OrderModel>();

                        return customer;
                    }
                );

            //Add new customer
            _customerServiceMock.Setup(s => s.Add(It.IsAny<CustomerModel>()))
                .Returns<CustomerModel>(customer =>
                {
                    _customerModels.Add(customer);
                    return customer.Id;
                });

            _customerServiceMock.Setup(s => s.AddOrderToCustomer(It.IsAny<Guid>(), It.IsAny<OrderModel>()))
                .Callback<Guid, OrderModel>((id, order) =>
                {
                    //get customer for update its orders
                    var customerToUpdate = _customerModels.SingleOrDefault(m => m.Id == id);

                    //if customer is not found throw new exception
                    if (customerToUpdate == null) throw new Exception();

                    //make copy of current customer
                    var customer = customerToUpdate;

                    //remove all from list
                    _customerModels.Remove(customerToUpdate);

                    //if order id is empty set new Id
                    if (order.Id == Guid.Empty)
                        order.Id = Guid.NewGuid();

                    //get list of orders to update
                    var orders = customer.Orders?.ToList() ?? new List<OrderModel>();

                    //add new order to list
                    orders.Add(order);

                    //update customers orders list
                    customer.Orders = orders;

                    //save customer data with new orders
                    _customerModels.Add(customer);
                });
        }

        private Mock<ICustomersService> _customerServiceMock;

        private readonly ICollection<CustomerModel> _customerModels = FakeDataModels.CustomersList;

        private readonly ICollection<OrderModel> _ordersModel = FakeDataModels.OrdersList;

        private CustomersController Controller => new CustomersController(_customerServiceMock.Object);

        [Test]
        public void GetAllCustomers()
        {
            // Arrange
            var allCustomers = _customerModels;

            // Act
            var returnedCustomers = Controller.Get();

            // Assert
            var customerModels = returnedCustomers as IList<CustomerModel> ?? returnedCustomers.ToList();

            allCustomers.ShouldAllBeEquivalentTo(customerModels);

            foreach (var returnedCustomer in customerModels)
            {
                returnedCustomer.Orders.Should().BeNull();
            }
        }

        [Test]
        public void GetCustomerByIdWithOrdersIncluded()
        {
            // Arrange
            var customer = _customerModels.FirstOrDefault();
            customer.Should().NotBeNull();

            // Act
            var returnedCustomer = Controller.Get(customer.Id);

            // Assert
            returnedCustomer.Should().NotBeNull();
            returnedCustomer.Orders.ShouldAllBeEquivalentTo(_ordersModel);
        }

        [Test]
        public void AddNewCustomer()
        {
            // Arrange
            var customer = new CustomerModel
            {
                Id = Guid.NewGuid(),
                Email = "newlycreated@domain.com",
                Name = "newlycreated"
            };
            // Act
            var id = Controller.Post(customer);

            // Assert
            var found = _customerModels.SingleOrDefault(m => m.Id == customer.Id);

            found.Should().NotBeNull();

            id.ShouldBeEquivalentTo(customer.Id);

            customer.ShouldBeEquivalentTo(found);
        }

        [Test]
        public void UpdateCustomerWithNewOrder()
        {
            // Arrange
            var customer = _customerModels.FirstOrDefault();

            customer.Should().NotBeNull();

            var order = new OrderModel
            {
                Amount = 15.20m,
                CreatedDate = DateTimeOffset.UtcNow.AddMinutes(-30)
            };

            // Act
            Controller.Put(customer.Id, order);

            // Assert
            var found = _customerModels.SingleOrDefault(m => m.Id == customer.Id);

            found.Should().NotBeNull();

            found.Orders.Should().NotBeNull();

            var foundOrder =
                found.Orders.FirstOrDefault(o => (o.Amount == order.Amount) && (o.CreatedDate == order.CreatedDate));

            foundOrder.Should().NotBeNull();

            foundOrder.Id.Should().NotBeEmpty().Should().NotBe(Guid.Empty);

            foundOrder.ShouldBeEquivalentTo(order, opt => opt.Excluding(e => e.Id));
        }
    }
}