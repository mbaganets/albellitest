﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlbelliTestApp.Shared.Models;

namespace AlbelliTestApp.WebAPI.Tests.StubbedData
{
    class FakeDataModels
    {
        public static ICollection<CustomerModel> CustomersList => new List<CustomerModel>
            {
                new CustomerModel
                {
                    Id = Guid.NewGuid(),
                    Name = "Some Name 1",
                    Email = "email1@domain.com"
                },
                new CustomerModel
                {
                    Id = Guid.NewGuid(),
                    Name = "Some Name 2",
                    Email = "email2@domain.com"
                },
                new CustomerModel
                {
                    Id = Guid.NewGuid(),
                    Name = "Some Name 3",
                    Email = "email3@domain.com"
                },
            };

        public static ICollection<OrderModel> OrdersList =>
            new List<OrderModel>
            {
                new OrderModel
                {
                    Id = Guid.NewGuid(),
                    Amount = 10.50m,
                    CreatedDate = DateTimeOffset.UtcNow
                },
                new OrderModel
                {
                    Id = Guid.NewGuid(),
                    Amount = 100.50m,
                    CreatedDate = DateTimeOffset.UtcNow.AddHours(-2)
                }
            };
    }
}
