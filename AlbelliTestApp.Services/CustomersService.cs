﻿using System;
using System.Collections.Generic;
using System.Linq;
using AlbelliTestApp.DataAccess.Interfaces;
using AlbelliTestApp.DataAccess.Models;
using AlbelliTestApp.Shared.Interfaces;
using AlbelliTestApp.Shared.Models;
using AutoMapper;
using AutoMapper.Execution;

namespace AlbelliTestApp.Services
{
    public class CustomersService : ICustomersService
    {
        private readonly IMapper _mapper;

        private readonly IExtendedRepository<Customer> _customersRepository;

        private readonly IGenericRepository<Order> _orderRepository;

        public CustomersService(
            IExtendedRepository<Customer> customersRepository,
            IGenericRepository<Order> orderRepository
        )
        {
            _customersRepository = customersRepository;
            _orderRepository = orderRepository;

            var config = new MapperConfiguration(cfg =>
            {
                //map Order model to Order entity
                cfg.CreateMap<OrderModel, Order>();

                //map Order entity to Order model
                cfg.CreateMap<Order, OrderModel>();

                //map Customer model to Customer entity
                cfg.CreateMap<CustomerModel, Customer>();

                //map Customer entity to Customer model
                cfg.CreateMap<Customer, CustomerModel>().ForMember(dest => dest.Orders, opt =>
                        opt.MapFrom(src => MapOrders(src)));
            });

            //create mapper using config
            _mapper = config.CreateMapper();
        }

        public CustomerModel GetById(Guid customerId)
        {
            //call getById method with FALSE param to do not retrieve orders
            return GetById(customerId, false);
        }

        public IEnumerable<CustomerModel> GetAll()
        {
            try
            {
                //get all customers from storage
                var customers = _customersRepository.GetAll();

                //return mapped to Model entity collections
                return _mapper.Map<ICollection<Customer>, ICollection<CustomerModel>>(customers);
            }
            catch (Exception)
            {
                //TODO: Log exception somehow   
                throw;
            }
        }

        public Guid Add(CustomerModel customer)
        {
            try
            {
                //convert Model to Entity
                var customerEntity = _mapper.Map<Customer>(customer);

                if(customerEntity.Id == Guid.Empty)
                    customerEntity.Id = Guid.NewGuid();

                //insert customer to storage
                var addedCustomer = _customersRepository.Insert(customerEntity);

                //return mapped to Model entity
                return customerEntity.Id;
            }
            catch (Exception)
            {
                //TODO: Log exception somehow   
                throw;
            }
        }

        public void Update(CustomerModel customer)
        {
            try
            {
                throw new NotImplementedException();
            }
            catch (Exception)
            {
                //TODO: Log exception somehow   
                throw;
            }
        }

        public void Delete(CustomerModel customer)
        {
            try
            {
                throw new NotImplementedException();
            }
            catch (Exception)
            {
                //TODO: Log exception somehow   
                throw;
            }

        }

        public void AddOrderToCustomer(Guid customerId, OrderModel order)
        {
            try
            {
                //convert Model to Entity
                var orderEntity = _mapper.Map<Order>(order);

                //if order has no Id, lets create it
                if (orderEntity.Id == Guid.Empty)
                    orderEntity.Id = Guid.NewGuid();

                if(orderEntity.CreatedDate == DateTimeOffset.MinValue)
                    orderEntity.CreatedDate = DateTimeOffset.UtcNow;

                //add order to customer
                orderEntity.CustomerId = customerId;

                //update customer in storage
                _orderRepository.Insert(orderEntity);
            }
            catch (Exception)
            {
                //TODO: Log exception somehow   
                throw;
            }
        }

        public CustomerModel GetById(Guid customerId, bool includeOrders)
        {
            try
            {
                //get customer by it with includeOrders parameter
                var customer = GetCustomerById(customerId, includeOrders);

                //return mapped to Model entity
                return _mapper.Map<CustomerModel>(customer);
            }
            catch (Exception)
            {
                //TODO: Log exception somehow   
                throw;
            }
        }

        private Customer GetCustomerById(Guid customerId, bool includeOrders)
        {
            try
            {
                //based on includeOrders param call generic GetById method without Orders
                //or call extended GetById method with INCLUDES param to query DB to retrieve linked Orders for Customer
                var customer = includeOrders
                    ? _customersRepository.GetById(customerId,
                        c => c.Orders
                    )
                    : _customersRepository.GetById(customerId);

                return customer;
            }
            catch (Exception)
            {
                //TODO: Log exception somehow   
                throw;
            }
        }
        #region MapperHelpers
        private IEnumerable<OrderModel> MapOrders(Customer customer)
        {
            try
            {
                return _mapper.Map<ICollection<OrderModel>>(customer.Orders);
            }
            catch (Exception)
            {
                return new HashSet<OrderModel>();
            }
        }
        #endregion
    }
}