﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlbelliTestApp.Shared.Models
{
    [DataContract]
    public class CustomerModel : BaseModel
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public IEnumerable<OrderModel> Orders { get; set; }
    }
}
