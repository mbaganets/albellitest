﻿using System;
using System.Runtime.Serialization;

namespace AlbelliTestApp.Shared.Models
{
    [DataContract]
    public class BaseModel
    {
        [DataMember]
        public Guid Id { get; set; }
    }
}
