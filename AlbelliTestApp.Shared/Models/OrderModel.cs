﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlbelliTestApp.Shared.Models
{
    [DataContract]
    public class OrderModel : BaseModel
    {
        [DataMember]
        public decimal Amount { get; set; }

        [DataMember]
        public DateTimeOffset CreatedDate { get; set; }
    }
}
