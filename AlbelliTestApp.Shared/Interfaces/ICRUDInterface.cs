﻿using System;
using System.Collections.Generic;
using AlbelliTestApp.Shared.Models;

namespace AlbelliTestApp.Shared.Interfaces
{
    public interface ICRUDInterface<TModel> where TModel : BaseModel
    {
        /// <summary>
        /// Get Model Item by Id
        /// </summary>
        /// <param name="modelItemId"></param>
        /// <returns>Found Model Item</returns>
        TModel GetById(Guid modelItemId);

        /// <summary>
        /// Get all Model Items
        /// </summary>
        /// <returns> collection of Model Items</returns>
        IEnumerable<TModel> GetAll();

        /// <summary>
        /// Add Model Item
        /// </summary>
        /// <param name="modelItem"></param>
        /// <returns>Id of added item</returns>
        Guid Add(TModel modelItem);

        /// <summary>
        /// Update Model Item
        /// </summary>
        /// <param name="modelItem"></param>
        void Update(TModel modelItem);

        /// <summary>
        /// Delete Model Item
        /// </summary>
        /// <param name="customerItem"></param>
        void Delete(TModel customerItem);
    }
}