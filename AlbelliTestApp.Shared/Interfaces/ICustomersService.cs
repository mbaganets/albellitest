﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlbelliTestApp.Shared.Models;

namespace AlbelliTestApp.Shared.Interfaces
{
    public interface ICustomersService : ICRUDInterface<CustomerModel>
    {
        /// <summary>
        /// Add Order to customer by customers Id
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="order"></param>
        void AddOrderToCustomer(Guid customerId, OrderModel order);

        /// <summary>
        /// Get Customer data with param, have it to include customers Orders or no
        /// if includeOrders is false Orders going to be empty set always
        /// if includeOrders is true Orders will be filled by Orders of customer, if customer has no orders it going to be empty set
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="includeOrders"></param>
        /// <returns></returns>
        CustomerModel GetById(Guid customerId, bool includeOrders);
    }
}
