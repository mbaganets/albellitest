using System;
using System.Data.Entity.Migrations;
using AlbelliTestApp.DataAccess.Contexts;
using AlbelliTestApp.DataAccess.Models;

namespace AlbelliTestApp.DataAccess.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<AlbelliTestAppContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(AlbelliTestAppContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.Customers.AddOrUpdate(
                new Customer
                {
                    Id = new Guid("07e2d447-bd76-4bf3-9a94-7bd0ae47fa4f"),
                    Name = "Some Name 1",
                    Email = "email1@domain.com"
                },
                new Customer
                {
                    Id = new Guid("e007ab76-4ac3-4e45-8267-16e841cc7636"),
                    Name = "Some Name 2",
                    Email = "email2@domain.com"
                },
                new Customer
                {
                    Id = new Guid("37f0081d-b7dd-4b1c-bff4-5a68381fa228"),
                    Name = "Some Name 3",
                    Email = "email3@domain.com"
                }
            );

            context.Orders.AddOrUpdate(
                new Order
                {
                    Id = new Guid("494862cf-26a2-414b-95d1-6b59f7c83a4b"),
                    CustomerId = new Guid("07e2d447-bd76-4bf3-9a94-7bd0ae47fa4f"),
                    Amount = 10.50m,
                    CreatedDate = new DateTimeOffset(2016, 10, 13, 14, 0, 0, 0, new TimeSpan(3, 0, 0))
                },
                new Order
                {
                    Id = new Guid("74ac1af6-1c63-4aa5-acee-e61cbcd62bd7"),
                    CustomerId = new Guid("07e2d447-bd76-4bf3-9a94-7bd0ae47fa4f"),
                    Amount = 100.50m,
                    CreatedDate = new DateTimeOffset(2016, 10, 13, 12, 0, 0, 0, new TimeSpan(3, 0, 0))
                }
            );

            context.SaveChanges();
        }
    }
}