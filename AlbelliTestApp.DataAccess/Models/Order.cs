﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlbelliTestApp.DataAccess.Models
{
    public class Order : BaseIdEntity
    {
        /// <summary>
        /// Gets or sets amount of order.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Gets or sets created date.
        /// </summary>
        public DateTimeOffset CreatedDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid CustomerId { get; set; }

        /// <summary>
        /// Gets or sets customer who has this order.
        /// </summary>
        public virtual Customer Customer { get; set; }
    }
}
