﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AlbelliTestApp.DataAccess.Models
{
    public class BaseIdEntity : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column("Id", TypeName = "uniqueidentifier")]
        public Guid Id { get; set; }
    }
}