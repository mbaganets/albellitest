﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AlbelliTestApp.DataAccess.Models
{
    public class Customer : BaseIdEntity
    {
        public Customer()
        {
            Orders = new HashSet<Order>();
        }

        /// <summary>
        ///     Gets or sets the name of customer.
        /// </summary>
        [StringLength(256)]
        public string Name { get; set; }

        /// <summary>
        ///     Gets or sets the email of customer.
        /// </summary>
        [StringLength(256)]
        public string Email { get; set; }

        /// <summary>
        ///     Gets or sets list of related orders.
        /// </summary>
        public virtual ICollection<Order> Orders { get; set; }
    }
}