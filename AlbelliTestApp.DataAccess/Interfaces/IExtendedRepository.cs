﻿using System;
using System.Linq.Expressions;
using AlbelliTestApp.DataAccess.Models;

namespace AlbelliTestApp.DataAccess.Interfaces
{
    public interface IExtendedRepository<TEntity> : IGenericRepository<TEntity> where TEntity : BaseEntity
    {
        /// <summary>
        ///     Get Entity by Id with Includes params
        /// </summary>
        /// <param name="id">Entity Id to find</param>
        /// <param name="includes"> linq expression to include nested navigation properties </param>
        /// <returns>Found Entity</returns>
        TEntity GetById(Guid id, params Expression<Func<TEntity, object>>[] includes);
    }
}