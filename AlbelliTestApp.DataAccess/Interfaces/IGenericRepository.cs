﻿using System;
using System.Collections.Generic;
using AlbelliTestApp.DataAccess.Models;

namespace AlbelliTestApp.DataAccess.Interfaces
{
    public interface IGenericRepository<TEntity> where TEntity : BaseEntity
    {
        /// <summary>
        /// Insert new Entity to storage
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Inserted Entity</returns>
        TEntity Insert(TEntity entity);

        /// <summary>
        /// Update Entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Updated Entity</returns>
        TEntity Update(TEntity entity);

        /// <summary>
        /// Get Entity by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Found Entity</returns>
        TEntity GetById(Guid id);

        /// <summary>
        /// Delete Entity from storage
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="key"></param>
        /// <returns>Code of success or failure operation</returns>
        int Delete(TEntity entity, Guid key);

        /// <summary>
        /// Get all Entities from storage
        /// </summary>
        /// <returns>Collection of Entities</returns>
        ICollection<TEntity> GetAll();
    }
}
