﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using AlbelliTestApp.DataAccess.Models;

namespace AlbelliTestApp.DataAccess.Interfaces
{
    public interface IDbContext : IDisposable
    {
        /// <summary>
        ///     Save changes
        /// </summary>
        /// <returns>return code of success or failure operation result</returns>
        int SaveChanges();

        /// <summary>
        ///     Get DbSet
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <returns>DbSet of Entities</returns>
        DbSet<TEntity> Entities<TEntity>() where TEntity : BaseEntity;

        /// <summary>
        ///     Get DbEntityEntry
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <returns>DbEntityEntry</returns>
        DbEntityEntry<TEntity> DbEntry<TEntity>(TEntity entity) where TEntity : BaseEntity;
    }
}