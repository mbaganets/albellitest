﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AlbelliTestApp.DataAccess.Interfaces;
using AlbelliTestApp.DataAccess.Models;
using AlbelliTestApp.Shared.Helpers;
using Dapper;

namespace AlbelliTestApp.DataAccess.Repositories.Dapper
{
    public class CustomersDapperRepository : BaseDapperRepository, IExtendedRepository<Customer>
    {
        private const string InsertQuery = "INSERT INTO Customers VALUES(@Id, @Name, @Email)";
        private const string RetrieveAllQuery = "select * from Customers";
        private const string RetrieveByIdQuery = "SELECT * FROM Customers WHERE Id = @id";

        private const string RetrieveByIdWithOrdersQuery =
            "SELECT * FROM Customers WHERE Id = @id; SELECT * FROM Orders WHERE CustomerId = @id";

        public Customer Insert(Customer entity)
        {
            if (entity.Id == Guid.Empty)
                entity.Id = Guid.NewGuid();

            Db.Execute(InsertQuery, new {id = entity.Id, entity.Name, entity.Email});

            return entity;
        }

        public Customer Update(Customer entity)
        {
            throw new NotImplementedException();
        }

        public Customer GetById(Guid id)
        {
            return Db.Query<Customer>(RetrieveByIdQuery, new {id}).SingleOrDefault();
        }

        public int Delete(Customer entity, Guid key)
        {
            throw new NotImplementedException();
        }

        public ICollection<Customer> GetAll()
        {
            return (List<Customer>) Db.Query<Customer>(RetrieveAllQuery);
        }

        public Customer GetById(Guid id, params Expression<Func<Customer, object>>[] includes)
        {
            if (!includes.Any(i => ExpressionTreeTest<Customer>.FuncEqual(i, c => c.Orders)))
                return GetById(id);

            using (var customerWithOrders = Db.QueryMultiple(RetrieveByIdWithOrdersQuery, new {id}))
            {
                var customer = customerWithOrders.Read<Customer>().SingleOrDefault();
                if (customer == null) return null;
                var orders = customerWithOrders.Read<Order>().ToList();
                if (!orders.Any()) return customer;
                customer.Orders = orders;
                return customer;
            }
        }
    }
}