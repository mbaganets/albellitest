﻿using System;
using System.Collections.Generic;
using AlbelliTestApp.DataAccess.Interfaces;
using AlbelliTestApp.DataAccess.Models;
using Dapper;

namespace AlbelliTestApp.DataAccess.Repositories.Dapper
{
    public class OrdersDapperRepository : BaseDapperRepository, IGenericRepository<Order>
    {
        private const string InsertQuery = "INSERT INTO Orders VALUES( @Id, @Amount, @CreatedDate, @CustomerId ) ";

        public Order Insert(Order entity)
        {
            if (entity.Id == Guid.Empty)
                entity.Id = Guid.NewGuid();

            Db.Execute(InsertQuery, new {id = entity.Id, entity.Amount, entity.CreatedDate, entity.CustomerId});

            return entity;
        }

        public Order Update(Order entity)
        {
            throw new NotImplementedException();
        }

        public Order GetById(Guid id)
        {
            throw new NotImplementedException();
        }

        public int Delete(Order entity, Guid key)
        {
            throw new NotImplementedException();
        }

        public ICollection<Order> GetAll()
        {
            throw new NotImplementedException();
        }
    }
}