﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace AlbelliTestApp.DataAccess.Repositories.Dapper
{
    public abstract class BaseDapperRepository
    {
        protected static readonly IDbConnection Db =
            new SqlConnection(ConfigurationManager.ConnectionStrings["AlbelliTestAppDb"].ConnectionString);
    }
}