﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AlbelliTestApp.DataAccess.Interfaces;
using AlbelliTestApp.DataAccess.Models;

namespace AlbelliTestApp.DataAccess.Repositories
{
    public class ExtendedRepository<TEntity> : GenericRepository<TEntity>, IExtendedRepository<TEntity> where TEntity : BaseIdEntity
    {
        public ExtendedRepository(IContextFabric contextFabric) : base(contextFabric)
        {
        }

        public TEntity GetById(Guid id, params Expression<Func<TEntity, object>>[] includes)
        {
            TEntity entity;

            using (var ctx = ContextFabric.Context())
            {
                var query = ctx.Entities<TEntity>().AsQueryable();

                if (includes != null)
                    query = includes.Aggregate(query, (current, include) => current.Include(include));

                entity = query.SingleOrDefault(e => e.Id == id);
            }

            return entity;

        }
    }
}
