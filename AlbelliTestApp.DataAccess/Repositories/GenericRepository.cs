﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AlbelliTestApp.DataAccess.Interfaces;
using AlbelliTestApp.DataAccess.Models;

namespace AlbelliTestApp.DataAccess.Repositories
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : BaseIdEntity
    {
        protected readonly IContextFabric ContextFabric;

        public GenericRepository(IContextFabric contextFabric)
        {
            ContextFabric = contextFabric;
        }
        public int Delete(TEntity entity, Guid key)
        {
            throw new NotImplementedException();
        }

        public ICollection<TEntity> GetAll()
        {
            IList<TEntity> list;

            using (var ctx = ContextFabric.Context())
            {
                list = ctx.Entities<TEntity>().ToList();
            }

            return list;
        }

        public TEntity GetById(Guid id)
        {
            TEntity entity;

            using (var ctx = ContextFabric.Context())
            {
                entity = ctx.Entities<TEntity>().Find(id);
            }

            return entity;
        }

        public TEntity Insert(TEntity entity)
        {
            TEntity addedEntity;
            using (var ctx = ContextFabric.Context())
            {
                addedEntity = ctx.Entities<TEntity>().Add(entity);
                ctx.SaveChanges();
            }

            return addedEntity;
        }

        public TEntity Update(TEntity entity)
        {
            if (entity == null) return null;

            TEntity existing;
            using (var ctx = ContextFabric.Context())
            {
                existing = ctx.Entities<TEntity>().Find(entity.Id);

                if (existing == null) return null;

                ctx.DbEntry(existing).CurrentValues.SetValues(entity);

                ctx.DbEntry(existing).State = EntityState.Modified;

                ctx.SaveChanges();
            }

            return existing;
        }
    }
}
