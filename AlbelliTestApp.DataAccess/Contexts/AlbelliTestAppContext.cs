﻿using System;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using AlbelliTestApp.DataAccess.Interfaces;
using AlbelliTestApp.DataAccess.Models;

namespace AlbelliTestApp.DataAccess.Contexts
{
    public class AlbelliTestAppContext : DbContext, IDbContext
    {
        public AlbelliTestAppContext() : this("name=AlbelliTestAppDb")
        {
        }

        public AlbelliTestAppContext(string connectionString) : base(connectionString)
        {
            Configuration.AutoDetectChangesEnabled = false;
        }

        public virtual DbSet<Customer> Customers { get; set; }

        public virtual DbSet<Order> Orders { get; set; }

        public new int SaveChanges()
        {
            try
            {
                var res = base.SaveChanges();

                return res;
            }
            catch (DbEntityValidationException validationException)
            {
                var msg = string.Empty;

                //often in innerException
                //loop through all inner exceptions
                var e = validationException as Exception;
                while (e.InnerException != null)
                {
                    msg += Environment.NewLine + e.Message;
                    e = e.InnerException;
                }

                foreach (var error in validationException.EntityValidationErrors)
                    foreach (var err in error.ValidationErrors)
                        msg += Environment.NewLine + $"Property: {err.PropertyName} Error: {err.ErrorMessage}";
                var fail = new Exception(msg, validationException);
                //Debug.WriteLine(fail.Message, fail);
                throw fail;
            }
            catch (OptimisticConcurrencyException)
            {
                var context = ((IObjectContextAdapter) this).ObjectContext;

                var refreshableObjects = ChangeTracker.Entries().Select(c => c.Entity).ToList();
                context.Refresh(RefreshMode.ClientWins, refreshableObjects);

                SaveChanges();
            }
            catch (DbUpdateConcurrencyException concurrencyException)
            {
                var context = ((IObjectContextAdapter) this).ObjectContext;

                var refreshableObjects = ChangeTracker.Entries().Select(c => c.Entity).ToList();
                context.Refresh(RefreshMode.StoreWins, refreshableObjects);

                return SaveChanges();
            }
            catch (DbUpdateException updateException)
            {
                var msg = string.Empty;

                //often in innerException
                //loop through all inner exceptions
                var e = updateException as Exception;
                while (e.InnerException != null)
                {
                    msg += Environment.NewLine + e.Message;
                    e = e.InnerException;
                }

                //which exceptions does it relate to
                msg = updateException.Entries.Aggregate(msg,
                    (current, entry) => current + (Environment.NewLine + entry.Entity));
                var fail = new Exception(msg, updateException);
                //Debug.WriteLine(fail.Message, fail);
                throw fail;
            }
            catch (Exception ex)
            {
                var msg = string.Empty;

                //often in innerException
                var e = ex;
                while (e.InnerException != null)
                {
                    msg += Environment.NewLine + e.Message;
                    e = e.InnerException;
                }
                if (ex.InnerException != null)
                    msg += Environment.NewLine + ex.InnerException.Message;
                //which exceptions does it relate to
                var fail = new Exception(msg, ex);

                throw fail;
            }
            return -1;
        }


        public DbSet<TEntity> Entities<TEntity>() where TEntity : BaseEntity
        {
            return Set<TEntity>();
        }

        public DbEntityEntry<TEntity> DbEntry<TEntity>(TEntity entity) where TEntity : BaseEntity
        {
            return Entry(entity);
        }
    }
}