﻿using AlbelliTestApp.DataAccess.Interfaces;

namespace AlbelliTestApp.DataAccess.Contexts
{
    public class ContextFabric : IContextFabric
    {
        private readonly string _connectionString;

        public ContextFabric(string connectionString = null)
        {
            _connectionString = connectionString;
        }

        public IDbContext Context()
        {
            return !string.IsNullOrWhiteSpace(_connectionString) ? new AlbelliTestAppContext(_connectionString) : new AlbelliTestAppContext();
        }
    }
}
